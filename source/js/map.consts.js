var image_dir = 'img/elements/';
var image_hover_suffix = '_hover';
var default_image = image_dir + "cityboard_left.png";
var data = [{
    'type': 'обычный',
    'price': '15000 р/мес',
    'coverage': '15000 чел/день',
    'direction': 'лево',
    'infoForModal':
        {
            'title':
                'Рекламный щит “Алёша”',
            'size':
                '3x6 м',
            'address':
                'Пермь, ул. Мира, д. 2',
            'category':
                'ситиборд',
            'description':
                'В нашей компании этот щит называют Алешей. Мы придумали ему это имя, потому что на нем была размещена первая наша реклама ...',
            'coverage_full':
                '5000 чел/день, 450 000 чел/мес',
            'goods_images':
                ["img/news2.png"]
        }
},
    {
        'type': 'обычный',
        'price': '15000 р/мес',
        'coverage': '15000 чел/день',
        'direction': 'право',
        'infoForModal':
            {
                'title':
                    'Рекламный щит “Алёша”',
                'size':
                    '3x6 м',
                'address':
                    'Пермь, ул. Мира, д. 2',
                'category':
                    'билборд',
                'description':
                    'В нашей компании этот щит называют Алешей. Мы придумали ему это имя, потому что на нем была размещена первая наша реклама ...',
                'coverage_full':
                    '5000 чел/день, 450 000 чел/мес',
                'goods_images':
                    ["img/news2.png"]
            }
    },
    {
        'type': 'обычный',
        'price': '15000 р/мес',
        'coverage': '15000 чел/день',
        'direction': 'лево',
        'infoForModal':
            {
                'title':
                    'Рекламный щит “Алёша”',
                'size':
                    '3x6 м',
                'address':
                    'Пермь, ул. Мира, д. 2',
                'category':
                    'ситиформат',
                'description':
                    'В нашей компании этот щит называют Алешей. Мы придумали ему это имя, потому что на нем была размещена первая наша реклама ...',
                'coverage_full':
                    '5000 чел/день, 450 000 чел/мес',
                'goods_images':
                    ["img/news2.png"]
            }
    },
    {
        'type': 'обычный',
        'price': '15000 р/мес',
        'coverage': '15000 чел/день',
        'direction': 'лево',
        'infoForModal':
            {
                'title':
                    'Рекламный щит “Алёша”',
                'size':
                    '3x6 м',
                'address':
                    'Пермь, ул. Мира, д. 2',
                'category':
                    'билборд',
                'description':
                    'В нашей компании этот щит называют Алешей. Мы придумали ему это имя, потому что на нем была размещена первая наша реклама ...',
                'coverage_full':
                    '5000 чел/день, 450 000 чел/мес',
                'goods_images':
                    ["img/news3.png"]
            }
    },
    {
        'type': 'обычный',
        'price': '15000 р/мес',
        'coverage': '15000 чел/день',
        'direction': 'право',
        'infoForModal':
            {
                'title':
                    'Рекламный щит “Алёша”',
                'size':
                    '3x6 м',
                'address':
                    'Пермь, ул. Мира, д. 2',
                'category':
                    'пиллар',
                'description':
                    'В нашей компании этот щит называют Алешей. Мы придумали ему это имя, потому что на нем была размещена первая наша реклама ...',
                'coverage_full':
                    '5000 чел/день, 450 000 чел/мес',
                'goods_images':
                    ["img/news1.png"]
            }
    },
    {
        'type': 'обычный',
        'price': '15000 р/мес',
        'coverage': '15000 чел/день',
        'direction': 'лево',
        'infoForModal':
            {
                'title':
                    'Рекламный щит “Алёша”',
                'size':
                    '3x6 м',
                'address':
                    'Пермь, ул. Мира, д. 2',
                'category':
                    'пиллар',
                'description':
                    'В нашей компании этот щит называют Алешей. Мы придумали ему это имя, потому что на нем была размещена первая наша реклама ...',
                'coverage_full':
                    '5000 чел/день, 450 000 чел/мес',
                'goods_images':
                    ["img/news3.png"]
            }
    },
    {
        'type': 'обычный',
        'price': '15000 р/мес',
        'coverage': '15000 чел/день',
        'direction': 'право',
        'infoForModal':
            {
                'title':
                    'Рекламный щит “Алёша”',
                'size':
                    '3x6 м',
                'address':
                    'Пермь, ул. Мира, д. 2',
                'category':
                    'пиллар',
                'description':
                    'В нашей компании этот щит называют Алешей. Мы придумали ему это имя, потому что на нем была размещена первая наша реклама ...',
                'coverage_full':
                    '5000 чел/день, 450 000 чел/мес',
                'goods_images':
                    ["img/news1.png"]
            }
    },
    {
        'type': 'обычный',
        'price': '15000 р/мес',
        'coverage': '15000 чел/день',
        'direction': 'право',
        'infoForModal':
            {
                'title':
                    'Рекламный щит “Алёша”',
                'size':
                    '3x6 м',
                'address':
                    'Пермь, ул. Мира, д. 2',
                'category':
                    'билборд',
                'description':
                    'В нашей компании этот щит называют Алешей. Мы придумали ему это имя, потому что на нем была размещена первая наша реклама ...',
                'coverage_full':
                    '5000 чел/день, 450 000 чел/мес',
                'goods_images':
                    ["img/news2.png"]
            }
    },];

var coords = [
    [58.002206, 56.246778],
    [58.001789, 56.248711],
    [58.002556, 56.245340],
    [58.002601, 56.247851],
    [58.000276, 56.250082],
    [58.002972, 56.252835],
    [58.003246, 56.261645],
    [58.001326, 56.261496]
];

var images = [
    image_dir + "cityboard_left.png",
    image_dir + "billboard_right_hover.png",
    image_dir + "citiformat_left.png",
    image_dir + "billboard_left.png",
    image_dir + "pillar_right.png",
    image_dir + "pillar_left.png",
    image_dir + "pillar_right.png",
    image_dir + "billboard_right.png"

];

var names = {
    'билборд': {
        'лево': 'billboard_left',
        'право': 'billboard_right',
        'размер': [68, 111]
    },
    'пиллар': {
        'лево': 'pillar_left',
        'право': 'pillar_right',
        'размер': [47, 92]
    },
    'ситиформат': {
        'лево': 'citiformat_left',
        'право': 'citiformat_right',
        'размер': [42, 93]
    },
    'ситиборд': {
        'лево': 'cityboard_left',
        'право': 'cityboard_right',
        'размер': [54, 98]
    }
};

var iconImageSize = [
    [42, 77],
    [68, 111],
    [31, 67],
    [42, 77],
    [33, 66],
    [33, 66],
    [33, 66],
    [42, 77],
];