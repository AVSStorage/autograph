ymaps.ready(['Panel']).then(function () {
    // Создание карты.
    var myMap = new ymaps.Map("map", {
        // Координаты центра карты.
        // Порядок по умолчанию: «широта, долгота».
        // Чтобы не определять координаты центра карты вручную,
        // воспользуйтесь инструментом Определение координат.
        center: [58.002410, 56.246839],
        // Уровень масштабирования. Допустимые значения:
        // от 0 (весь мир) до 19.
        zoom: 17
    });


    // Создадим и добавим панель на карту.

    var panel = new ymaps.Panel();
    myMap.controls.add(panel, {
        float: 'left'
    });
    // Создадим коллекцию геообъектов.
    var collection = new ymaps.GeoObjectCollection(null, {
        // Запретим появление балуна.
        hasBalloon: false,
        // iconColor: '#3b5998'
    });


    // Создание макета содержимого хинта.
    // Макет создается через фабрику макетов с помощью текстового шаблона.
    HintLayout = ymaps.templateLayoutFactory.createClass( "<div class='hint'>" +
        "<p>охват:<span>{{ properties.coverage}}</span></p>" +
        "<p>тип:<span>{{ properties.type }}</span></p>" +
        "<p><strong>цена:</strong><span><strong>{{ properties.price }}</strong></span></p>" +
        "</div>", {
            // Определяем метод getShape, который
            // будет возвращать размеры макета хинта.
            // Это необходимо для того, чтобы хинт автоматически
            // сдвигал позицию при выходе за пределы карты.
            getShape: function () {
                var el = this.getElement(),
                    result = null;
                if (el) {
                    var firstChild = el.firstChild;
                    result = new ymaps.shape.Rectangle(
                        new ymaps.geometry.pixel.Rectangle([
                            [0, 0],
                            [firstChild.offsetWidth, firstChild.offsetHeight]
                        ])
                    );
                }
                return result;
            }
        }
    );



    for (var i = 0; i < coords.length; i++) {
        collection.add(new ymaps.Placemark(coords[i], data[i] , {
            // Опции.
            //     // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: images[i],
            // Размеры метки.
            iconImageSize: iconImageSize[i],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-5, -38],
            hintLayout: HintLayout
        }));
    }

    myMap.geoObjects.add(collection);

    function getObjectName(categories, category, el_direction) {
        for (var name in categories) {
            if (name === category) {
                for (var direction in categories[name]) {
                    if (direction === el_direction) {
                        return [image_dir +  categories[name][direction], categories[name] ];
                    }
                }
            }
        }
    }
    // Подпишемся на событие клика по коллекции.
    collection.events.add('click', function (e) {
        // Получим ссылку на геообъект, по которому кликнул пользователь.
        var target = e.get('target');

       // сгенерируем путь до большой иконки
         var image_name = getObjectName(names,target.properties.get('infoForModal').category,target.properties.get('direction'))[0];

         if (image_name) {
             var image = image_name +image_hover_suffix + '.png';
         } else {
             image = default_image;
         }


        // создадим шаблон для модального окна
        var template = panel.setTemplate(target.properties.get('infoForModal'),  target.properties.get('type'), target.properties.get('price'), image);
        // Зададим контент боковой панели.
        panel.setContent(template);

        // Переместим центр карты по координатам метки с учётом заданных отступов.
        myMap.panTo(target.geometry.getCoordinates(), {useMapMargin: true});
    });



    collection.events.add('mouseenter', function (e) {
        // Ссылку на объект, вызвавший событие,
        // можно получить из поля 'target'.
        var image_name = getObjectName(names, e.get('target').properties.get('infoForModal').category, e.get('target').properties.get('direction'))[0];
        var image_size = getObjectName(names, e.get('target').properties.get('infoForModal').category, e.get('target').properties.get('direction'))[1]['размер'];

        var bigImage = image_name ? image_name + image_hover_suffix+ '.png': default_image;

// Открываем балун в центре карты.

        e.get('target').options.set('iconImageHref', bigImage);
        e.get('target').options.set('iconImageOffset', [-10, -76]);
        e.get('target').options.set('iconImageSize',  image_size);

    });
    collection.events.add('mouseleave', function (e) {
        var image_name = getObjectName(names, e.get('target').properties.get('infoForModal').category, e.get('target').properties.get('direction'))[0];
        var normalImage = image_name ? image_name + image_hover_suffix+ '.png': default_image;
        e.get('target').options.set('iconImageHref', normalImage);
        e.get('target').options.set('iconImageOffset', [-5, -38]);
        e.get('target').options.set('iconImageSize',  [42, 77]);
    });

});