$(".slider").slick({

    // normal options...
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    variableWidth: true,
    prevArrow: "<img src='https://svgshare.com/i/6Ei.svg' class='slick-prev slick-prev-logo' alt='1'>",
    nextArrow: "<img src='https://svgshare.com/i/6Gf.svg' class='slick-next slick-prev-logo' alt='2'>",
    // the magic
    responsive: [{

        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            infinite: true,
        }

    },{

        breakpoint: 768,
        settings: {
            slidesToShow: 2,
            variableWidth: false
        }

    }, {

        breakpoint: 576,
        settings: {
            slidesToShow: 1,
            variableWidth: false
        }

    },
        {

        breakpoint: 300,
        settings: "unslick" // destroys slick

    }]
});