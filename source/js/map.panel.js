// Пример реализации боковой панели на основе наследования от collection.Item.
// Боковая панель отображает информацию, которую мы ей передали.
ymaps.modules.define('Panel', [
    'util.augment',
    'collection.Item'
], function (provide, augment, item) {
    // Создаем собственный класс.
    var Panel = function (options) {
        Panel.superclass.constructor.call(this, options);
    };

    // И наследуем его от collection.Item.
    augment(Panel, item, {
        onAddToMap: function (map) {
            Panel.superclass.onAddToMap.call(this, map);
            this.getParent().getChildElement(this).then(this._onGetChildElement, this);
            // Добавим отступы на карту.
            // Отступы могут учитываться при установке текущей видимой области карты,
            // чтобы добиться наилучшего отображения данных на карте.
            map.margin.addArea({
                top: 0,
                left: 100,
                width: '500px',
                height: '100%'
            })
        },

        setTemplate: function (info, type, price, good_image) {
            var images = '';

            for (var i = 0; i < 5; i++) {
                images += '<img class="modal__slide" src="' + info.goods_images[0] + '" width="119" height="120">';
            }

            return '<h2 class="modal__title">' + info.title + '</h2>' +

                '<div class="modal__wrapper">' +
                '<table class="modal__description">' +
                '<thead><tr><td>' +
                '<h3 class="modal__subtitle">ХАРАКТЕРИСТИКИ:</h3>' +
                '</td></tr></thead>' +
                '<tr><td><strong>Размер:</strong></td><td>' + info.size + '</td></tr>' +
                '<tr><td><strong>Тип:</strong></td><td>' + type + '</td></tr>' +
                '<tr><td><strong>Охват:</strong></td><td>' + info.coverage_full + '</td></tr>' +
                '<tr><td><strong>Адрес:</strong></td><td>' + info.address + '</td></tr>' +
                '<tr><td><strong>Цена:</strong></td><td>' + price + '</td></tr>' +
                '<tr><td><strong>Описание:</strong></td></tr>' +
                '<tr><td><p>' + info.description + '</p></td></tr>' +
                '</table>' +
                '<div class="modal__wrapper-right">' +
                '<div class="modal__good"> <span>' + info.category + '</span> ' +
                '<img class="modal__image" src="'+ good_image +'">' + '</div>' +
                '</div>' +
                '</div>' +
                '<div class="modal-slider">' + images +
                '</div>' +
                '<a class="modal__link" href="#">Подробнее</a>';
        },

        onRemoveFromMap: function (oldMap) {
            if (this._$control) {
                this._$control.remove();
            }
            Panel.superclass.onRemoveFromMap.call(this, oldMap);
        },

        _onGetChildElement: function (parentDomContainer) {
            // Создаем HTML-элемент с текстом.
            // По-умолчанию HTML-элемент скрыт.
            this._$control = $('<div class="customControl modal"><div class="content"></div><div class="closeButton modal-close"></div></div>').appendTo(parentDomContainer);
            this._$content = $('.content');
            // При клике по крестику будем скрывать панель.
            $('.closeButton').on('click', this._onClose);
        },
        _onClose: function () {
            $('.customControl').css('display', 'none');
        },
        // Метод задания контента панели.
        setContent: function (text) {
            // При задании контента будем показывать панель.

            this._$control.css('display', 'flex');



            if (window.matchMedia('(max-width: 576px)').matches)
            {
                this._$control.css({
                    'width': '86%',
                    'height': '607px',
                    'padding-left': '14px',
                    'right': '0px',
                    'padding-right': '14px',
                    'padding-top': '37px',
                    'top': '-12px',
                    'border': '4px solid #c9efbb',
                });
            } else {
                this._$control.css({
                    'width': '434px',
                    'height': '607px',
                    'padding-left': '37px',
                    'padding-right': '37px',
                    'padding-top': '37px',
                    'top': '-12px',
                    'border': '4px solid #c9efbb',
                });
            }
            this._$content.html(text);
            $(".modal-slider").slick({

                // normal options...
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                variableWidth: true,
                slide: ".modal__slide",
                rows: 0,
                prevArrow: "<img src='img/elements/slider-prev.png' class='slick-prev-modal slick-prev ' alt='1'>",
                nextArrow: "<img src='img/elements/slider-next.png' class='slick-next-modal slick-next ' alt='2'>",
                // the magic
                responsive: [{

                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        infinite: true,
                    }

                }, {

                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        arrows: false
                    }

                }, {

                    breakpoint: 300,
                    settings: "unslick" // destroys slick

                }]
            });
        }
    });

    provide(Panel);
});